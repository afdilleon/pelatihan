class PesertaFields {
  static String tableName = 'peserta';
  static String idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
  static String textType = 'TEXT NOT NULL';
  static String intType = 'INTEGER NOT NULL';
  static const String id = 'id';
  static const String nama = 'nama';
  static const String email = 'email';
  static const String noHp = 'number';

  static List<String> values = [id, nama, email, noHp];
}

class PesertaModel {
  final int? id;
  final String nama;
  final String email;
  final int? noHp;

  PesertaModel({
    this.id,
    required this.nama,
    required this.email,
    this.noHp,
  });

  PesertaModel copy({
    int? id,
    String? nama,
    String? email,
    int? noHp,
  }) {
    return PesertaModel(
        id: id ?? this.id,
        nama: nama ?? this.nama,
        email: email ?? this.email,
        noHp: noHp ?? this.noHp);
  }

  Map<String, dynamic> toJson() {
    return {
      PesertaFields.id: id,
      PesertaFields.nama: nama,
      PesertaFields.email: email,
      PesertaFields.noHp: noHp,
    };
  }

  factory PesertaModel.fromJson(Map<String, Object?> json) {
    return PesertaModel(
      id: json['id'] as int,
      nama: json[PesertaFields.nama] as String,
      email: json[PesertaFields.email] as String,
      noHp: json[PesertaFields.noHp] as int?,
    );
  }
}
