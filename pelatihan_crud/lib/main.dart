import 'package:flutter/material.dart';
import 'package:pelatihan_crud/pesert_form.dart';
import 'package:pelatihan_crud/peserta_db.dart';
import 'package:pelatihan_crud/peserta_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
        dividerTheme: DividerThemeData(
          color: Colors.transparent,
        ),
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => HomeScreen(),
        '/form': (context) => PesertForm(),
      },
    );
  }
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final PesertaDb pesertaDb = PesertaDb.instance;
  List<PesertaModel> peserta = [];
  bool loading = false;

  @override
  void initState() {
    getPeserta();
    super.initState();
  }

  @override
  void dispose() {
    pesertaDb.close();
    super.dispose();
  }

  void getPeserta() async {
    setState(() {
      loading = true;
    });
    var result = await pesertaDb.readAll();
    setState(() {
      peserta = result;
      loading = false;
    });
  }

  void hapusPeserta(PesertaModel data, context) async {
    setState(() {
      loading = true;
    });
    var result = await pesertaDb.delete(data);
    setState(() {
      loading = false;
    });
    if (result == 1) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Peserta berhasil dihapus.'),
          );
        },
      ).then((value) {
        getPeserta();
      });
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Peserta Gagal dihapus.'),
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget contents = Center(
      child: Text('Peserta masih kosong'),
    );
    if (peserta.isNotEmpty) {
      contents = CustomScrollView(
        slivers: [
          SliverList.separated(
            itemBuilder: (context, index) {
              return Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Card(
                  child: ListTile(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return PesertForm(
                              peserta: peserta[index],
                            );
                          },
                        ),
                      );
                    },
                    title: Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    Text("Nama :"),
                                    VerticalDivider(),
                                    Text(peserta[index].nama),
                                  ],
                                ),
                              ),
                              Container(
                                child: Row(
                                  children: [
                                    Text("Email :"),
                                    VerticalDivider(),
                                    Text(peserta[index].email),
                                  ],
                                ),
                              ),
                              Container(
                                child: Row(
                                  children: [
                                    Text("No HP :"),
                                    VerticalDivider(),
                                    Text(peserta[index].noHp != null
                                        ? peserta[index].noHp.toString()
                                        : '-'),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: IconButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: Text(
                                        'Apakah yakin untuk mengahpus data?.'),
                                    actions: [
                                      ElevatedButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: Text('Tidak'),
                                      ),
                                      ElevatedButton(
                                        onPressed: () {
                                          Navigator.pop(context);
                                          hapusPeserta(peserta[index], context);
                                        },
                                        style: ElevatedButton.styleFrom(
                                          backgroundColor: Colors.green,
                                        ),
                                        child: Text(
                                          'Ya',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                            icon: Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return Divider(
                color: Colors.transparent,
              );
            },
            itemCount: peserta.length,
          )
        ],
      );
    }
    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.indigo,
            title: Text('List Peserta'),
          ),
          body: RefreshIndicator(
            onRefresh: () async {
              getPeserta();
            },
            child: contents,
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.pushNamed(context, '/form');
            },
            child: Icon(Icons.add),
          ),
        ),
        if (loading)
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: Colors.black54,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ),
      ],
    );
  }
}
