import 'package:flutter/material.dart';
import 'package:pelatihan_crud/main.dart';
import 'package:pelatihan_crud/peserta_db.dart';
import 'package:pelatihan_crud/peserta_model.dart';

class PesertForm extends StatefulWidget {
  const PesertForm({
    super.key,
    this.peserta,
  });
  final PesertaModel? peserta;

  @override
  State<PesertForm> createState() => _PesertFormState();
}

class _PesertFormState extends State<PesertForm> {
  final PesertaDb pesertaDb = PesertaDb.instance;
  final TextEditingController nama = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController noHP = TextEditingController();
  bool loading = false;

  @override
  void initState() {
    if (widget.peserta != null) {
      nama.text = widget.peserta!.nama;
      email.text = widget.peserta!.email;
      noHP.text =
          widget.peserta!.noHp != null ? widget.peserta!.noHp.toString() : '';
    }
    super.initState();
  }

  @override
  void dispose() {
    pesertaDb.close();
    super.dispose();
  }

  void createPeserta(context) async {
    var result = await pesertaDb.create(PesertaModel(
      nama: nama.text,
      email: email.text,
      noHp: int.tryParse(noHP.text),
    ));
    if (result.id != null) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) {
            return HomeScreen();
          },
        ),
        (route) => false,
      );
    }
  }

  void updatePeserta(context) async {
    var result = await pesertaDb.update(PesertaModel(
      id: widget.peserta!.id,
      nama: nama.text,
      email: email.text,
      noHp: int.tryParse(noHP.text),
    ));
    if (result == 1) {
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (context) {
            return HomeScreen();
          },
        ),
        (route) => false,
      );
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Gagal Update'),
          );
        },
      );
    }
  }

  void showError(context, String field) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Ada kesalah.'),
          content: Text('$field Belum diisi'),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Form Peserta'),
          backgroundColor: Colors.indigo,
        ),
        body: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: CustomScrollView(
                slivers: [
                  SliverList.list(
                    children: [
                      Text(
                        'Nama',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextField(
                        controller: nama,
                        decoration: InputDecoration(
                          hintText: 'Masukkan Nama',
                        ),
                      ),
                      Divider(),
                      Text(
                        'email',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextField(
                        controller: email,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Masukkan email',
                        ),
                      ),
                      Divider(),
                      Text(
                        'No HP',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      TextField(
                        controller: noHP,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: 'Masukkan No HP',
                        ),
                      ),
                      Divider(),
                    ],
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      padding: EdgeInsets.all(20),
                      child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            loading = true;
                          });
                          if (nama.text.isEmpty) {
                            showError(context, 'Nama');
                          } else if (email.text.isEmpty) {
                            showError(context, 'Email');
                          } else {
                            if (widget.peserta != null) {
                              updatePeserta(context);
                            } else {
                              createPeserta(context);
                            }
                          }

                          setState(() {
                            loading = false;
                          });
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.indigo,
                        ),
                        child: Text(
                          'Simpan',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if (loading)
              Container(
                color: Colors.black54,
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              )
          ],
        ),
      ),
    );
  }
}
