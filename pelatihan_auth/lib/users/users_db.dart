import 'package:cloud_firestore/cloud_firestore.dart';

class UserDb {
  static final FirebaseFirestore store = FirebaseFirestore.instance;

  Future<dynamic> addUser(String nama, String email, String nohp) async {
    CollectionReference users = store.collection('users');

    try {
      var res = users.add({
        'nama': nama,
        'email': email,
        'nohp': nohp,
        'kelas': 'TI 06',
      });

      return res;
    } catch (e) {
      return e.toString();
    }
  }

  Future<dynamic> getUser() async {
    CollectionReference users = store.collection('users');

    try {
      var res = users.get();

      return res;
    } catch (e) {
      return e.toString();
    }
  }

  Future<dynamic> updateUser(
      String userId, String nama, String email, String nohp) async {
    CollectionReference users = store.collection('users');

    try {
      var res = await users.doc(userId).update({
        'nama': nama,
        'email': email,
        'nohp': nohp,
      });

      return res;
    } catch (e) {
      return e.toString();
    }
  }

  Future<dynamic> deleteUser(String userId) async {
    CollectionReference users = store.collection('users');

    try {
      await users.doc(userId).delete();

      return "User berhasil di hapus";
    } catch (e) {
      return e.toString();
    }
  }
}
