import 'package:flutter/material.dart';
import 'package:latihan_auth/users/users_db.dart';

class LoginSuccess extends StatelessWidget {
  const LoginSuccess({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                alignment: Alignment.center,
                child: ElevatedButton(
                  onPressed: () {
                    UserDb()
                        .addUser('test', 'admin@mail.com', '12335321')
                        .then((value) {
                      print(value);
                    });
                  },
                  child: Text('Test input'),
                ),
              )
            ],
          )),
    );
  }
}
