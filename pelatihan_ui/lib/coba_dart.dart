void main() async {
  List test = [
    '1',
    '2',
    '3',
  ];
  print("Hello world");

  for (var item in test) {
    print(item);
  }

  test.forEach((item) {
    print(int.tryParse(item));
    item = int.tryParse(item) ?? 0 + 1;
  });

  print('hasil mapping');

  test = test.map((item) {
    item = (int.tryParse(item) ?? 0) + 1;
    return item;
  }).toList();

  for (var item in test) {
    print(item);
  }
  print('hasil dari function');

  test = test.map((item) {
    kalkulasi(item).then((value) {
      return item = value;
    });
    return item;
  }).toList();

  print('hasil dari function await');

  print(await kalkulasi(10));

  List listItem = [
    {
      'nama': 'afdil',
      'nomor': '001',
      'kunci': true,
    },
    {
      'nama': 'afdil1',
      'nomor': '001',
      'kunci': false,
    }
  ];
  Map mapItem = {
    'nama': 'afdil',
    'nomor': '001',
    'kunci': true,
  };

  print(listItem.where((item) {
    if (item['kunci']) {
      return true;
    } else {
      return false;
    }
  }));

  print(mapItem.containsKey('kunci'));
}

Future<int> kalkulasi(item) async {
  Future.delayed(Duration(seconds: 1));
  int hasil = item + 2;
  return hasil;
}
