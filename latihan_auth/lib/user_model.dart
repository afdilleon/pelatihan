import 'package:firebase_auth/firebase_auth.dart';

class UserModel {
  final User? user;
  final String? message;

  UserModel({required this.user, required this.message});
}
