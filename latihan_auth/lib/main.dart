import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:latihan_auth/firebase_options.dart';
import 'package:latihan_auth/login_success.dart';
import 'package:latihan_auth/user_model.dart';
import 'package:latihan_auth/users/users_db.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(
    GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  theme() {
    ThemeData baseTheme = ThemeData(
      useMaterial3: true,
    );

    return baseTheme.copyWith(
      appBarTheme: const AppBarTheme(
        color: Colors.indigo,
      ),
      dividerTheme: baseTheme.dividerTheme.copyWith(
        color: Colors.transparent,
      ),
      scaffoldBackgroundColor: Colors.white,
    );
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: theme(),
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(),
        '/login_success': (context) => LoginSuccess(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  FirebaseAuth auth = FirebaseAuth.instance;

  final TextEditingController cEmail = TextEditingController();
  final TextEditingController cPassword = TextEditingController();
  bool loading = false;

  Future<UserModel> sigInWithEmail(
      {required String email, required String pass}) async {
    try {
      setState(() {
        loading = true;
      });
      UserCredential res =
          await auth.signInWithEmailAndPassword(email: email, password: pass);
      setState(() {
        loading = false;
      });
      return UserModel(user: res.user, message: null);
    } catch (e) {
      setState(() {
        loading = false;
      });
      return UserModel(user: null, message: e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.blue.shade400,
          body: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.all(20),
                  child: Form(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Email',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        TextField(
                          controller: cEmail,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                              ),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                              ),
                            ),
                            hintText: 'Masukkan Email anda',
                            hintStyle: TextStyle(
                              color: Colors.white70,
                            ),
                          ),
                        ),
                        Divider(),
                        Text(
                          'Password',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        TextField(
                          controller: cPassword,
                          obscureText: true,
                          decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                              ),
                            ),
                            border: UnderlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,
                              ),
                            ),
                            hintText: 'Masukkan Password anda',
                            hintStyle: TextStyle(
                              color: Colors.white70,
                            ),
                          ),
                        ),
                        Divider(),
                        SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: ElevatedButton(
                            onPressed: () {
                              sigInWithEmail(
                                      email: cEmail.text, pass: cPassword.text)
                                  .then((res) {
                                if (res.user != null) {
                                  Navigator.pushNamed(
                                      context, '/login_success');
                                } else {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text(
                                          'Error',
                                          style: TextStyle(
                                              color: Colors.red.shade700),
                                        ),
                                        content: Text(res.message!),
                                        actions: [
                                          ElevatedButton(
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                            child: Text('ok'),
                                          )
                                        ],
                                      );
                                    },
                                  );
                                }
                              });
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.indigo),
                            child: Text(
                              'Login',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        if (loading)
          Container(
            color: Colors.black54,
            height: MediaQuery.of(context).size.height,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
      ],
    );
  }
}
