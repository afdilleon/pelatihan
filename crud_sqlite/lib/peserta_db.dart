import 'dart:async';

import 'package:crud_sqlite/peserta_model.dart';
import 'package:sqflite/sqflite.dart';

class PesertaDb {
  static final PesertaDb instance = PesertaDb._internal();
  static Database? mydb;

  PesertaDb._internal();

  Future<Database> get database async {
    if (mydb != null) {
      return mydb!;
    } else {
      mydb = await _initDatabase();
      return mydb!;
    }
  }

  Future<Database> _initDatabase() async {
    final databasePath = await getDatabasesPath();
    final path = '$databasePath/peserta.db';
    return await openDatabase(
      path,
      version: 1,
      onCreate: createDatabase,
    );
  }

  FutureOr<void> createDatabase(Database db, int version) async {
    return await db.execute(
      'CREATE TABLE ${PesertaFields.tableName} (${PesertaFields.id} ${PesertaFields.idType}, ${PesertaFields.nama} ${PesertaFields.textType}, ${PesertaFields.email} ${PesertaFields.textType}, ${PesertaFields.noHp} INTEGER)',
    );
  }

  Future<PesertaModel> create(PesertaModel peserta) async {
    final db = await instance.database;
    final id = await db.insert(PesertaFields.tableName, peserta.toJson());
    return peserta.copy(id: id);
  }

  Future<PesertaModel> read(int id) async {
    final db = await instance.database;
    final res = await db.query(
      PesertaFields.tableName,
      columns: PesertaFields.values,
      where: '${PesertaFields.id} = ',
      whereArgs: [id],
    );
    if (res.isNotEmpty) {
      return PesertaModel.fromJson(res.first);
    } else {
      throw Exception('Peserta id $id tidak ditemukan');
    }
  }

  Future<List<PesertaModel>> readAll() async {
    final db = await instance.database;
    final res = await db.query(
      PesertaFields.tableName,
      orderBy: '${PesertaFields.id} ASC',
    );

    if (res.isNotEmpty) {
      return res.map((json) => PesertaModel.fromJson(json)).toList();
    } else {
      return [];
    }
  }

  Future<int> update(PesertaModel peserta) async {
    final db = await instance.database;
    final res = await db.update(
      PesertaFields.tableName,
      peserta.toJson(),
      where: '${PesertaFields.id} = ',
      whereArgs: [peserta.id],
    );
    return res;
  }

  Future<int> delete(PesertaModel peserta) async {
    final db = await instance.database;
    final res = await db.delete(
      PesertaFields.tableName,
      where: '${PesertaFields.id} = ',
      whereArgs: [peserta.id],
    );
    return res;
  }

  Future close() async {
    final db = await instance.database;
    db.close();
  }
}
