import 'package:crud_sqlite/peserta_db.dart';
import 'package:crud_sqlite/peserta_model.dart';
import 'package:flutter/material.dart';

class PendaftaranPeserta extends StatefulWidget {
  const PendaftaranPeserta({super.key});

  @override
  State<PendaftaranPeserta> createState() => _PendaftaranPesertaState();
}

class _PendaftaranPesertaState extends State<PendaftaranPeserta> {
  final TextEditingController nama = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController noHp = TextEditingController();
  bool loading = false;
  PesertaDb pesertaDb = PesertaDb.instance;

  @override
  void dispose() {
    pesertaDb.close();
    super.dispose();
  }

  Future<PesertaModel> createPeserta() async {
    var res = await pesertaDb.create(PesertaModel(
        nama: nama.text, email: email.text, noHp: int.tryParse(noHp.text)));
    return res;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pendaftaran Peserta'),
      ),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Nama',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      TextField(
                        controller: nama,
                        decoration: InputDecoration(
                          hintText: 'Nama',
                        ),
                      ),
                      Divider(),
                      Text(
                        'Email',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      TextField(
                        controller: email,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          hintText: 'email',
                        ),
                      ),
                      Divider(),
                      Text(
                        'No HP',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      TextField(
                        controller: noHp,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: 'no hp',
                        ),
                      ),
                      Divider(),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Divider(),
              ),
              SliverToBoxAdapter(
                child: Container(
                  padding: EdgeInsets.all(20),
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() {
                        loading = true;
                      });

                      if (nama.text.isEmpty) {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text('Ada kesalahan.'),
                              content: Text('Field nama harus diisi'),
                              actions: [
                                ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text('oke'),
                                ),
                              ],
                            );
                          },
                        );
                      } else if (email.text.isEmpty) {
                        showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: Text('Ada kesalahan.'),
                              content: Text('Field email harus diisi'),
                              actions: [
                                ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text('oke'),
                                ),
                              ],
                            );
                          },
                        );
                      }

                      createPeserta().then(
                        (value) {
                          print(value);
                        },
                      );

                      setState(() {
                        loading = false;
                      });
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.indigo,
                    ),
                    child: Text(
                      'Simpan',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
          if (loading)
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            )
        ],
      ),
    );
  }
}
