import 'package:crud_sqlite/pendaftaran_peserta.dart';
import 'package:crud_sqlite/peserta_db.dart';
import 'package:crud_sqlite/peserta_model.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Latihan Sqlite',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
        dividerColor: Colors.transparent,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(),
        '/pendaftaran': (context) => PendaftaranPeserta(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PesertaDb pesertaDb = PesertaDb.instance;
  List<PesertaModel> listPeserta = [];

  @override
  void initState() {
    getPeserta();
    super.initState();
  }

  @override
  void dispose() {
    pesertaDb.close();
    super.dispose();
  }

  void getPeserta() async {
    var res = await pesertaDb.readAll();
    setState(() {
      listPeserta = res;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget contents = SliverFillRemaining(
      child: Center(
        child: Text('Data peserta masih kosong'),
      ),
    );
    if (listPeserta.isNotEmpty) {
      contents = SliverList.separated(
        itemBuilder: (context, index) {
          return ListTile(
            title: Text("Nama Peserta"),
          );
        },
        separatorBuilder: (context, index) {
          return Divider();
        },
        itemCount: listPeserta.length,
      );
    }
    return Scaffold(
      appBar: AppBar(
        title: Text('Daftar Peserta'),
      ),
      body: CustomScrollView(
        slivers: [
          contents,
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/pendaftaran');
        },
        tooltip: 'Tambah Peserta',
        child: const Icon(Icons.add),
      ),
    );
  }
}
